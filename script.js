/*
  Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. 
Потрібно, щоб після натискання на вкладку відображався 
конкретний текст для потрібної вкладки. При цьому решта тексту 
повинна бути прихована. У коментарях зазначено, який текст має 
відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, 
і що вкладки можуть додаватися та видалятися. При цьому потрібно, 
щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
*/

"use strict";
const tabs = document.querySelectorAll(".tabs-container .tabs");
tabs.forEach((tab) => {
  tab.addEventListener("click", tabChange);
});

function tabChange({ target }) {
  const currentTab = target.closest(".tabs-title");
  if (!currentTab) return;
  const tabsContainer = currentTab.closest(".tabs-container");
  const activeTabId = currentTab.dataset.tabId;
  const toggleActiveState = (element) => {
    element.dataset.tabId !== activeTabId
      ? element.classList.remove("active")
      : element.classList.add("active");
  };
  tabsContainer.querySelectorAll(".tabs-title").forEach((tab) => {
    toggleActiveState(tab);
  });
  tabsContainer.querySelectorAll(".tab-content").forEach((content) => {
    toggleActiveState(content);
  });
}
